﻿using System;
using System.Collections.Generic;
using System.Text;

namespace agendatech_dotnet.BLL
{
    public class Endereco
    {
        private string logradouro;
        private string complemento;
        private string estado;

        public Endereco()
        {
        }

        public Endereco(string logradouro, string complemento, string estado)
        {
            this.logradouro = logradouro;
            this.complemento = complemento;
            this.estado = estado;
        }

        public Endereco(string estado)
        {
            this.estado = estado;
        }

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }

    }
}
