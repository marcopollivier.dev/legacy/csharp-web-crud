﻿using System;
using System.Collections.Generic;
using System.Text;

namespace agendatech_dotnet.BLL
{
    public class Processo
    {
        private int id;
        private string numero;
        private DateTime dataCriacao;
        private decimal valor;
        private Cliente cliente;
        private Endereco endereco;
        private SituacaoDoProcesso situacao;

        public Processo()
        {
            situacao = SituacaoDoProcesso.Ativo;
        }

        public Processo(int id, string numero, DateTime dataCriacao, Cliente cliente, string logradouro, string complemento, string estado, decimal valor)
        {
            situacao = SituacaoDoProcesso.Ativo;
            this.id = id;
            this.numero = numero;
            this.dataCriacao = dataCriacao;
            this.cliente = cliente;
            this.valor = valor;
            endereco = new Endereco(logradouro, complemento, estado);
        }

        public Processo(int id, string numero, DateTime dataCriacao, Cliente cliente, string estado, decimal valor)
        {
            situacao = SituacaoDoProcesso.Ativo;
            this.id = id;
            this.numero = numero;
            this.dataCriacao = dataCriacao;
            this.cliente = cliente;
            this.valor = valor;
            endereco = new Endereco(estado);
        }

        public Processo(int id, string numero, DateTime dataCriacao, Cliente cliente, string estado, decimal valor, SituacaoDoProcesso situacao)
        {
            situacao = SituacaoDoProcesso.Ativo;
            this.id = id;
            this.numero = numero;
            this.dataCriacao = dataCriacao;
            this.cliente = cliente;
            this.valor = valor;
            this.situacao = situacao;
            endereco = new Endereco(estado);
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public DateTime DataCriacao
        {
            get { return dataCriacao; }
            set { dataCriacao = value; }
        }

        public Endereco Endereco
        {
            get { return endereco; }
        }

        public void DefineEndereco(string logradouro, string complemento, string estado)
        {
            endereco = new Endereco(logradouro, complemento, estado);
        }

        public SituacaoDoProcesso Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

    }
}
