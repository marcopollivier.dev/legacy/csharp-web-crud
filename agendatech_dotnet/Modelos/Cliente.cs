﻿using System;
using System.Collections.Generic;
using System.Text;

namespace agendatech_dotnet.BLL
{
    public class Cliente
    {
        private int id;
        private string cnpj;
        private string nome;
        private Endereco endereco = null;
                
        public Cliente()
        {
            endereco = new Endereco();
        }

        public Cliente(int id, string cnpj, string nome, string logradouro, string complemento, string estado)
        {
            this.id = id;
            this.cnpj = cnpj;
            this.nome = nome;
            endereco = new Endereco(logradouro, complemento, estado);
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Cnpj
        {
            get { return cnpj; }
            set { cnpj = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public Endereco Endereco
        {
            get { return endereco; }
        }

        public void DefineEndereco(string logradouro, string complemento, string estado)
        {
            endereco = new Endereco(logradouro, complemento, estado);
        }

    }
}
