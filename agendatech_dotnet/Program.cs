using System;
using System.Collections.Generic;
using System.Text;
using agendatech_dotnet.BLL.Repositorios;

namespace agendatech_dotnet.BLL
{
    class Program
    {
        static void Main(string[] args)
        {
            //formatando
            RepositorioDeClientes repoClientes = new RepositorioDeClientes();

            string atual = repoClientes.BuscarPorID(1).Nome;
            Console.WriteLine(atual);

            atual = repoClientes.BuscarPorID(2).Nome;
            Console.WriteLine(atual);

        }
    }
}
