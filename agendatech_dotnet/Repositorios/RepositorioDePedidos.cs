﻿using System;
using System.Collections.Generic;
using System.Text;

namespace agendatech_dotnet.BLL.Repositorios
{
    public class RepositorioDePedidos : IRepositorioGenerico<Processo>
    {

        List<Processo> processos = null;
        RepositorioDeClientes clientes;

        public RepositorioDePedidos()
        {
            processos = new List<Processo>();
            GerarDadosMock();
        }

        private void GerarDadosMock()
        {
            clientes = new RepositorioDeClientes();
            Adicionar(new Processo(1, "00001CIVELRJ", new DateTime(2007, 10, 10), clientes.BuscarPorID(1), "Rio de Janeiro", 200000M));
            Adicionar(new Processo(2, "00002CIVELSP", new DateTime(2007, 10, 20), clientes.BuscarPorID(1), "São Paulo", 100000M));
            Adicionar(new Processo(3, "00003TRABMG", new DateTime(2007, 10, 30), clientes.BuscarPorID(1), "Minas Gerais", 10000M, SituacaoDoProcesso.Inativo));
            Adicionar(new Processo(4, "00004CIVELRJ", new DateTime(2007, 11, 10), clientes.BuscarPorID(1), "Rio de Janeiro", 20000M, SituacaoDoProcesso.Inativo));
            Adicionar(new Processo(5, "00005CIVELSP", new DateTime(2007, 11, 15), clientes.BuscarPorID(1), "Rio de Janeiro", 35000M));
                
            Adicionar(new Processo(6, "00006CIVELRJ", new DateTime(2007, 5, 1), clientes.BuscarPorID(2), "Rio de Janeiro", 20000M));
            Adicionar(new Processo(7, "00007CIVELRJ", new DateTime(2007, 6, 2), clientes.BuscarPorID(2), "Rio de Janeiro", 700000M));
            Adicionar(new Processo(8, "00008CIVELSP", new DateTime(2007, 7, 3), clientes.BuscarPorID(2), "São Paulo", 500M, SituacaoDoProcesso.Inativo));
            Adicionar(new Processo(9, "00009CIVELSP", new DateTime(2007, 8, 4), clientes.BuscarPorID(2), "São Paulo", 32000M));
            Adicionar(new Processo(10, "00010TRABAM", new DateTime(2007, 9, 5), clientes.BuscarPorID(2), "Amazonas", 1000M, SituacaoDoProcesso.Inativo));
        }

        public List<Processo> ObterTodos()
        {
            return processos;
        }

        public Processo BuscarPorID(int id)
        {
            foreach (Processo processo in processos)
            {
                if (processo.Id == id)
                    return processo;
            }

            return null;
        }

        public void Adicionar(Processo processo)
        {
            if (processo != null)
            {
                processos.Add(processo);
            }
        }


    }
}
