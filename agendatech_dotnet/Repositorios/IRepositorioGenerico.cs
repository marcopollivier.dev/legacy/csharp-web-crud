﻿using System;
using System.Collections.Generic;
using System.Text;

namespace agendatech_dotnet.BLL.Repositorios
{
    public interface IRepositorioGenerico<T>
    {
        List<T> ObterTodos();
        T BuscarPorID(int id);
        void Adicionar(T obj);
    }
}
