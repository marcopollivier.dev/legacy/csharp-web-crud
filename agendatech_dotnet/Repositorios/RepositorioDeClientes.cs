﻿using System;
using System.Collections.Generic;
using System.Text;

namespace agendatech_dotnet.BLL.Repositorios
{
    public class RepositorioDeClientes : IRepositorioGenerico<Cliente>
    {

        List<Cliente> clientes = null;

        public RepositorioDeClientes()
        {
            clientes = new List<Cliente>();
            GerarDadosMock();
        }

        private void GerarDadosMock()
        {
            Adicionar(new Cliente(1, "00000000001", "Empresa A", "Rua São José", "2o Andar", "Rio de Janeiro"));
            Adicionar(new Cliente(2, "00000000002", "Empresa B", "Av. São João", "22o Andar", "São Paulo"));
        }

        public List<Cliente> ObterTodos()
        {
            return clientes;
        }

        public Cliente BuscarPorID(int id)
        {
            foreach (Cliente cliente in clientes)
            {
                if (cliente.Id == id)
                {
                    return cliente;
                }
            }

            return null;
        }

        public List<Cliente> BuscaPorEstado(string estado)
        {
            List<Cliente> lstCliente = new List<Cliente>();

            foreach (Cliente cliente in clientes)
            {
                if (cliente.Endereco.Estado == estado)
                {
                    lstCliente.Add(cliente);
                }
            }

            return lstCliente;
        }

        public void Adicionar(Cliente cliente)
        {
            if(cliente != null)
            {
                clientes.Add(cliente);
            }
        }

        public void teste()
        {

        }

    }
}
